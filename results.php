<!--
 　 · ✵						 　 · ✵					 　 · ✵					
 　　　　 ⋆ ✧　 　 · 　 ✧　✵	 　 · ✵					 　　　　 ⋆ ✧　 　 · 　 ✧　✵	
　　 ⋆ ✧　 　 · 　 ✧　✵		 。　☆ 。　　☆。　　☆ 	　　 ⋆ ✧　 　 · 　 ✧　✵		
 　 · ✵					★。　＼　　｜　　／。　★	 　 · ✵					
 　   *　　 * ⋆ 　 .		☆ 　　N E Y O S 		 　 　　 *　　 * ⋆ 　 .		
 · 　　 ⋆ 　　　 ˚ ˚ 　✦★。　／　　｜　　＼。　★  · 　　 ⋆ 　　　 ˚ ˚ 　　 ✦	
 　 ⋆ · 　 *				。　☆。 　　。　　☆。	 　 ⋆ · 　 *				
 　　　　 ⋆ ✧　 　 · 　 ✧　✵	 　 · ✵					　　　　 ⋆ ✧　 　 · 　 ✧　✵	
 　 · ✵					　 · ✵				 　 · ✵						
		Encryptie van heden is niet meer als toen en is toe aan vernieuwing.
		Author:	Anouar Allouti
-->
<?php

	include "PHP_LIBARY/db_config.php";
	
	include "PHP_LIBARY/valideer_sessie.php";
	
	include "PHP_LIBARY/update_account_actief.php";
	
	include "PHP_LIBARY/update_ip_recent.php";
	
	include "PHP_LIBARY/update_date_recent.php";

?>
<?php

	//ophalen gebruikers_id voor <div id="liveberichten">
	$result = mysqli_query($connection, "SELECT ID FROM users WHERE email = '$email'");
	if (!$result) {
		echo 'Could not run query: ' . mysqli_error();
		exit;
	}
	
	$row = mysqli_fetch_row($result);

?>
<?php

	//ontvangen input van chat.php
	$nickname = $_POST["nickname"];
	$nickname = preg_replace("#[^0-9a-z]#i","",$nickname);
		
	$query = "SELECT id, nickname, foto FROM users WHERE nickname LIKE '$nickname%' AND actief='1'";
	$result = mysqli_query($connection, $query);
		
	//valideer input van chat.php
	if (!$nickname) {
    
		header("Location: zoek");
	
	}
		
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Neyos ~ Resultaten</title>
		<link rel="shortcut icon" href="xoneyos.ico">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="main2.css">
	</head>
	<body class="bg">
	
	<div align="center">
	
		<form>
		
			<img src="logo.png" alt="Logo isn't showing" class="logo">
			
			<span style="font-family:'Trebuchet MS', Helvetica, sans-serif; font-size:20px; align:center;">Resultaten</span>
			
			<div id="liveberichten">
				<script>
			
				function liveberichten(){
					xmlhttp = new XMLHttpRequest();
					xmlhttp.open("GET","live_berichtenhome?id=<?php echo $row[0]; ?>",false);
					xmlhttp.send(null);
				
					document.getElementById("liveberichten").innerHTML = xmlhttp.responseText;
				}
			
				liveberichten();
				
				setInterval(function(){
					liveberichten();
				},6000);
				
				</script>
			</div>
			
			<hr style="opacity:0;">
			
			<?php 
					$hit = 0;
					
				
					// als resultaten
					while($gegevens = mysqli_fetch_row($result)){
							
							$hit = 1;
							
							$id 			= $gegevens[0];
							$naam 			= $gegevens[1];
							$foto 			= $gegevens[2];
							
							echo "<script>	function info$id(){ location.href='info?id=$id' }	</script>";
							$button1		= "<input type='button' name='info' value='Info' onclick='info$id();' style='width: 50px; background-color: #00e6e6;'/>";
							
							echo "<script>	function chat$id(){ location.href='chat?id=$id' }	</script>";
							$button2		= "<input type='button' name='chat' value='Chat' onclick='chat$id();' style='width: 50px; background-color:#ffff00;'/>";
							
							echo "<script>	function blok$id(){ location.href='blok?id=$id' }	</script>";
							$button3		= "<input type='button' name='blok' value='Blok' onclick='blok$id();' style='width: 50px; background-color: #ff4d4d;'/>";
							
							//foto check
							if($gegevens[2] == ""){
								$foto = "<img src='default.jpg' alt='Foto is niet beschikbaar' style='width: 100px; border: 1px solid black;	border-radius: 4px 4px 4px 4px;' />";
							} else {
								$foto = "<img src='fotos/".$gegevens[2]."' alt='Foto is niet beschikbaar' style='width: 100px; border: 1px solid black;	border-radius: 4px 4px 4px 4px;' />";
							}
							
							
							
								echo "<p align='center'> $naam </p>";
								echo "<span align='center'> $foto </span>";
								
								echo "<br>";
								
								echo "<span align='center'>$button1 $button2 $button3</span>";
								
								echo "<br>";
							
							
							}
							
							
					// als geen resultaten
					if($hit == 0){
						
							echo "<p>	Geen resultaten voor: '$nickname'	</p>";
							
					} else {
						
						// niks
						
					}
			?>
			
			<br>
			
			<input type="button" name="terug" value="Terug" onclick="location.href='zoek'" style="width: 180px;"/>
			
		</form>

		<br><br>
		
	</div>
		
	</body>
</html>