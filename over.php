<!--
 　 · ✵						 　 · ✵					 　 · ✵					
 　　　　 ⋆ ✧　 　 · 　 ✧　✵	 　 · ✵					 　　　　 ⋆ ✧　 　 · 　 ✧　✵	
　　 ⋆ ✧　 　 · 　 ✧　✵		 。　☆ 。　　☆。　　☆ 	　　 ⋆ ✧　 　 · 　 ✧　✵		
 　 · ✵					★。　＼　　｜　　／。　★	 　 · ✵					
 　   *　　 * ⋆ 　 .		☆ 　　N E Y O S 		 　 　　 *　　 * ⋆ 　 .		
 · 　　 ⋆ 　　　 ˚ ˚ 　✦★。　／　　｜　　＼。　★  · 　　 ⋆ 　　　 ˚ ˚ 　　 ✦	
 　 ⋆ · 　 *				。　☆。 　　。　　☆。	 　 ⋆ · 　 *				
 　　　　 ⋆ ✧　 　 · 　 ✧　✵	 　 · ✵					　　　　 ⋆ ✧　 　 · 　 ✧　✵	
 　 · ✵					　 · ✵				 　 · ✵						
		Encryptie van heden is niet meer als toen en is toe aan vernieuwing.
		Author:	Anouar Allouti
-->
<!DOCTYPE html>
<html>
	<head>
		<title>Neyos ~ Over</title>
		<link rel="shortcut icon" href="xoneyos.ico">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="main2.css">
	</head>
	<body class="bg">
	
	<div align="center">
	
		<form>
		
		<img src="logo.png" alt="Logo isn't showing" class="logo">
		
		<p class="text">Neyos is afgeleid van het griekse term "néos"</p>
		<p class="text">Dat vertaald in het Nederlands "nieuw" betekent.</p>
		
		<br>
		
		<p class="text">Neyos stelt privacy als fundamenteel voor derde.</p>
		<p class="text">Ons communicatieplatform handeert een eigen ontwikkelde symmetrische cryptografie,</p>
		<p class="text">en occupeert zich voortdurend met het back-end codering.</p>
		
		<br>
		
		<p class="text">Encryptie van heden is niet meer als toen en is toe aan vernieuwing.</p>
		
		<br>
		
		<p class="text">~ Neyos</p>
		
		<input type="button" name="terug" value="Terug" onclick="location.href='inloggen'" style="width: 180px;"/>
		
		</form>
		
	</div>
		
	</body>
</html>