<!--
 　 · ✵						 　 · ✵					 　 · ✵					
 　　　　 ⋆ ✧　 　 · 　 ✧　✵	 　 · ✵					 　　　　 ⋆ ✧　 　 · 　 ✧　✵	
　　 ⋆ ✧　 　 · 　 ✧　✵		 。　☆ 。　　☆。　　☆ 	　　 ⋆ ✧　 　 · 　 ✧　✵		
 　 · ✵					★。　＼　　｜　　／。　★	 　 · ✵					
 　   *　　 * ⋆ 　 .		☆ 　　N E Y O S 		 　 　　 *　　 * ⋆ 　 .		
 · 　　 ⋆ 　　　 ˚ ˚ 　✦★。　／　　｜　　＼。　★  · 　　 ⋆ 　　　 ˚ ˚ 　　 ✦	
 　 ⋆ · 　 *				。　☆。 　　。　　☆。	 　 ⋆ · 　 *				
 　　　　 ⋆ ✧　 　 · 　 ✧　✵	 　 · ✵					　　　　 ⋆ ✧　 　 · 　 ✧　✵	
 　 · ✵					　 · ✵				 　 · ✵						
		Encryptie van heden is niet meer als toen en is toe aan vernieuwing.
		Author:	Anouar Allouti
-->
<?php

	include "PHP_LIBARY/db_config.php";
	
	include "PHP_LIBARY/valideer_sessie.php";
	
	include "PHP_LIBARY/update_account_actief.php";
	
	include "PHP_LIBARY/update_ip_recent.php";
	
	include "PHP_LIBARY/update_date_recent.php";
	
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Neyos ~ VVB</title>
		<link rel="shortcut icon" href="xoneyos.ico">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="main2.css">
	</head>
	<body class="bg">
	
	<div align="center">
	
		<form name="submit" method="POST" enctype="multipart/form-data">
		
			<img src="logo.png" alt="Logo isn't showing" class="logo">
			
			<p style="font-size:10px;">Bij het voltooien van dit formulier kunnen al uw verstuurde berichten <span style="color:red;font-weight:bold;">niet</span> meer worden hersteld, en zal dit niet meer zichtbaar zijn in zowel uw perspectief als van de ontvanger.</p>
			<br>
			
			<input required type="password"
			name="password" placeholder="wachtwoord" style="width: 180px;"/><br>

			<input required type="password"
			name="reppassword" placeholder="herhaal wachtwoord" style="width: 180px;"/><br>
			
			<input type="submit"
			name="submit" value="Verwijder" style="width: 79px;" />
			
			<input type="button"
			name="annuleren" value="Annuleren" onclick="location.href='home'" style="width: 79px;"/>
			
			<p style="color: #FF0000;" id="melding"></p>
			
		</form>

	</div>
		
	</body>
</html>
<?php

	if(isset($_POST['submit'])) {
		
		$password 		= mysqli_real_escape_string($connection, $_POST['password']);
		$reppassword 	= mysqli_real_escape_string($connection, $_POST['reppassword']);
		
		// check of velden ingevuld zijn
		if(empty($_POST["password"]) || empty($_POST["reppassword"]))
		{  
	
           echo "<script>document.getElementById('melding').innerHTML = 'Veld(en) niet ingevuld.'</script>"; 
		   exit();
		   
		} else {
			
			// niks
			
		}
		
		// check of wachtwoord overeen komt met herhaal wachtwoord veld
		if ($password !== $reppassword){
		
		echo "<script>document.getElementById('melding').innerHTML = 'Wachtwoorden komen niet overeen.'</script>";
		exit();
		
		} else {
		
			// niks
		
		}
		
		include "sleutels/kryptos.php";
		
		// check of wachtwoord juist is
		$query = "SELECT password FROM users WHERE email = '$email'";
		$result = mysqli_query($connection, $query);
		$ftch = mysqli_fetch_row($result);
		
		if ($ftch[0] !== $password){
		
		echo "<script>document.getElementById('melding').innerHTML = 'Wachtwoord onjuist.'</script>";
		exit();
		
		} else {
		
			// niks
			
		}
		
		// als bovenste 3 processen correct afgehandelt zijn worden verstuurde berichten verwijdert
		$query = "SELECT id FROM users WHERE email = '$email'";
		$run = mysqli_query($connection, $query);
		
		$id_verzender = mysqli_fetch_row($run);
		
		$query = "DELETE FROM msg WHERE id_verzender = '$id_verzender[0]'";
		$run = mysqli_query($connection, $query);
		
		
		echo "
	  
				<script>
	  
				msg = alert('Verstuurde berichten verwijdert!');
				if (msg = true){
					location.href='home'
				} else {
					location.href='home' }
			
				</script>
	  
		";
		
	}

?>