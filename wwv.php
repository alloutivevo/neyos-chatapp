<!--
 　 · ✵						 　 · ✵					 　 · ✵					
 　　　　 ⋆ ✧　 　 · 　 ✧　✵	 　 · ✵					 　　　　 ⋆ ✧　 　 · 　 ✧　✵	
　　 ⋆ ✧　 　 · 　 ✧　✵		 。　☆ 。　　☆。　　☆ 	　　 ⋆ ✧　 　 · 　 ✧　✵		
 　 · ✵					★。　＼　　｜　　／。　★	 　 · ✵					
 　   *　　 * ⋆ 　 .		☆ 　　N E Y O S 		 　 　　 *　　 * ⋆ 　 .		
 · 　　 ⋆ 　　　 ˚ ˚ 　✦★。　／　　｜　　＼。　★  · 　　 ⋆ 　　　 ˚ ˚ 　　 ✦	
 　 ⋆ · 　 *				。　☆。 　　。　　☆。	 　 ⋆ · 　 *				
 　　　　 ⋆ ✧　 　 · 　 ✧　✵	 　 · ✵					　　　　 ⋆ ✧　 　 · 　 ✧　✵	
 　 · ✵					　 · ✵				 　 · ✵						
		Encryptie van heden is niet meer als toen en is toe aan vernieuwing.
		Author:	Anouar Allouti
-->
<?php

	include "PHP_LIBARY/db_config.php";
	
	include "PHP_LIBARY/valideer_sessie.php";
	
	include "PHP_LIBARY/update_account_actief.php";
	
	include "PHP_LIBARY/update_ip_recent.php";
	
	include "PHP_LIBARY/update_date_recent.php";
	
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Neyos ~ Wachtwoord</title>
		<link rel="shortcut icon" href="xoneyos.ico">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="main2.css">
	</head>
	<body class="bg">
	
	<div align="center">
	
		<form name="submit" method="POST" enctype="multipart/form-data">
		
			<img src="logo.png" alt="Logo isn't showing" class="logo">
			
			<p style="font-size:10px;">Wachtwoord kan bij verlies <span style="color:red;font-weight:bold;">niet</span> worden hersteld door zowel de gebruiker als de administrator.</p>
			<br>
			
			<input required type="password"
			name="password" placeholder="huidige wachtwoord" style="width: 180px;"/><br>

			<input required type="password"
			name="newpassword" placeholder="nieuwe wachtwoord" style="width: 180px;"/><br>
			
			<input required type="password"
			name="reppassword" placeholder="herhaal wachtwoord" style="width: 180px;"/><br>
			
			<input type="submit"
			name="submit" value="Wijzigen" style="width: 79px;" />
			
			<input type="button"
			name="annuleren" value="Annuleren" onclick="location.href='home'" style="width: 79px;"/>
			
			<p style="color: #FF0000;" id="melding"></p>
			
		</form>

	</div>
		
	</body>
</html>
<?php

	if(isset($_POST['submit'])) {
		
		$password 		= mysqli_real_escape_string($connection, $_POST['password']);
		$newpassword 	= mysqli_real_escape_string($connection, $_POST['newpassword']);
		$reppassword 	= mysqli_real_escape_string($connection, $_POST['reppassword']);
		
		// check of velden ingevuld zijn
		if(empty($_POST["password"]) || empty($_POST["newpassword"]) || empty($_POST["reppassword"]))
		{  
	
           echo "<script>document.getElementById('melding').innerHTML = 'Veld(en) niet ingevuld.'</script>"; 
		   exit();
		   
		} else {
			
			// niks
			
		}
		
		include "sleutels/kryptos.php";
		
		// check of oude wachtwoord juist is
		$query = "SELECT password FROM users WHERE email = '$email'";
		$result = mysqli_query($connection, $query);
		$ftch = mysqli_fetch_row($result);
		
		if ($ftch[0] !== $password){
		
		echo "<script>document.getElementById('melding').innerHTML = 'Huidige wachtwoord onjuist.'</script>";
		exit();
		
		} else {
		
			// niks
			
		}
		
		// check of nieuw wachtwoord overeen komt met herhaal wachtwoord veld
		if ($newpassword !== $reppassword){
		
		echo "<script>document.getElementById('melding').innerHTML = 'Wachtwoorden komen niet overeen.'</script>";
		exit();
		
		} else {
		
			// niks
		
		}
		
		// als bovenste 3 processen correct afgehandelt zijn wordt wachtwoord gewijzigd
		$password = $newpassword;
		
		include "sleutels/kryptos.php";
		
		$query = "UPDATE users SET password = '$password' WHERE email = '$email'";
		$run = mysqli_query($connection, $query);
		
		echo "
	  
				<script>
	  
				msg = alert('Wachtwoord gewijzigd!');
				if (msg = true){
					location.href='logout'
				} else {
					location.href='logout' }
			
				</script>
	  
		";
		
	}

?>