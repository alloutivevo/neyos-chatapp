<?php

	include "PHP_LIBARY/db_config.php";
	
	include "PHP_LIBARY/valideer_sessie.php";
	
	include "PHP_LIBARY/update_account_actief.php";
	
	include "PHP_LIBARY/update_ip_recent.php";
	
	include "PHP_LIBARY/update_date_recent.php";
	
?>
<?php

	//ophalen van GET input
	$id = mysqli_real_escape_string($connection, $_GET["id"]);
	
	//filteren voor mogelijke SQL Injections
	
	if (strpos($id, 'a') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'A') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'i') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'I') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'e') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'E') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'o') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'O') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'u') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'U') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, '-') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, '*') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, ' ') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	$id = preg_replace("#[^0-9a-z]#i","",$id);
	
	//valideer of ID geldig
	$query = "SELECT * FROM users WHERE id = '$id'";
	$run = mysqli_query($connection, $query);
	$hit = mysqli_fetch_array($run);
	
	if ($hit < 1){
		Header('Location: error');
		exit;
	}
	
	//valideer of user actief
	$query = "SELECT actief FROM users WHERE id = '$id'";
	$run = mysqli_query($connection, $query);
	$hit = mysqli_fetch_array($run);
	
	if ($hit[0] == 0){
		Header('Location: error');
		exit;
	}
	
	//ophalen gebruikersgegevens
	$result = mysqli_query($connection, "SELECT nickname,foto,date_made,date_recent,ID,line FROM users WHERE id = '$id'");
	
	if (!$result){
		echo 'Could not run query: ' . mysqli_error();
		exit;
	}
	
	$row = mysqli_fetch_row($result);
	
	//formatteren date data
	$dm = date_create($row[2]);
	$ls = date_create($row[3]);

?>

<?php
	
	//ophalen id van ontvanger & verzender
	$sql_id = mysqli_query($connection, "SELECT id FROM users WHERE email = '$email'");
	$res_id = mysqli_fetch_row($sql_id);
		
	$id_verzender = $res_id[0];
	$id_ontvanger = $row[4];

?>

<?php

	include "PHP_LIBARY/valideer_blok.php";

?>

<?php
		
		if($row[1] == ""){
			
			// als gebruiker geen foto heeft
			if($row[5] == "0"){
				
				// als gebruiker offline zonder foto
				echo "<img src='default.jpg' title='$row[0] is offline' alt='Foto is niet beschikbaar' style='width: 75px; height: 75px; border: 3px solid #ff6666;	border-radius: 4px 4px 4px 4px;' />";
				
			} else if($row[5] == "1"){
				
				// als gebruiker online zonder foto
				echo "<img src='default.jpg' title='$row[0] is online' alt='Foto is niet beschikbaar' style='width: 75px; height: 75px; border: 3px solid #c4feb1;	border-radius: 4px 4px 4px 4px;' />";
				
			}
				
			// als gebruiker foto heeft
			} else {
				
				if($row[5] == "0"){
					
				// als gebruiker offline met foto
				echo "<img src='fotos/".$row[1]."' title='$row[0] is offline' alt='Foto is niet beschikbaar' style='width: 75px; height: 75px; border: 3px solid #ff6666;	border-radius: 4px 4px 4px 4px;' />";
				
				} else if($row[5] == "1"){
					
				// als gebruiker online met foto
				echo "<img src='fotos/".$row[1]."' title='$row[0] is online' alt='Foto is niet beschikbaar' style='width: 75px; height: 75px; border: 3px solid #c4feb1;	border-radius: 4px 4px 4px 4px;' />";
				
					
				}
				
			}
		
?>


<p style="font-size:11px;" id="ls">Laatst gezien: <?php echo date_format($ls, 'd/m/Y'); ?> om <?php echo date_format($ls, 'H:i'); ?></p>