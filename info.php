<!--
 　 · ✵						 　 · ✵					 　 · ✵					
 　　　　 ⋆ ✧　 　 · 　 ✧　✵	 　 · ✵					 　　　　 ⋆ ✧　 　 · 　 ✧　✵	
　　 ⋆ ✧　 　 · 　 ✧　✵		 。　☆ 。　　☆。　　☆ 	　　 ⋆ ✧　 　 · 　 ✧　✵		
 　 · ✵					★。　＼　　｜　　／。　★	 　 · ✵					
 　   *　　 * ⋆ 　 .		☆ 　　N E Y O S 		 　 　　 *　　 * ⋆ 　 .		
 · 　　 ⋆ 　　　 ˚ ˚ 　✦★。　／　　｜　　＼。　★  · 　　 ⋆ 　　　 ˚ ˚ 　　 ✦	
 　 ⋆ · 　 *				。　☆。 　　。　　☆。	 　 ⋆ · 　 *				
 　　　　 ⋆ ✧　 　 · 　 ✧　✵	 　 · ✵					　　　　 ⋆ ✧　 　 · 　 ✧　✵	
 　 · ✵					　 · ✵				 　 · ✵						
		Encryptie van heden is niet meer als toen en is toe aan vernieuwing.
		Author:	Anouar Allouti
-->
<?php

	include "PHP_LIBARY/db_config.php";
	
	include "PHP_LIBARY/valideer_sessie.php";
	
	include "PHP_LIBARY/update_account_actief.php";
	
	include "PHP_LIBARY/update_ip_recent.php";
	
	include "PHP_LIBARY/update_date_recent.php";
	
?>
<?php

	//ophalen van GET input
	$id = mysqli_real_escape_string($connection, $_GET["id"]);
	
	//filteren voor mogelijke SQL Injections
	
	if (strpos($id, 'a') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'A') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'i') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'I') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'e') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'E') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'o') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'O') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'u') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'U') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, '-') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, '*') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, ' ') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	$id = preg_replace("#[^0-9a-z]#i","",$id);
	
	//valideer of ID geldig
	$query = "SELECT * FROM users WHERE id = '$id'";
	$run = mysqli_query($connection, $query);
	$hit = mysqli_fetch_array($run);
	
	if ($hit < 1){
		Header('Location: error');
		exit;
	}
	
	//valideer of user actief
	$query = "SELECT actief FROM users WHERE id = '$id'";
	$run = mysqli_query($connection, $query);
	$hit = mysqli_fetch_array($run);
	
	if ($hit[0] == 0){
		Header('Location: error');
		exit;
	}
	
	//ophalen gebruikersgegevens
	$result = mysqli_query($connection, "SELECT nickname,foto,date_made,date_recent,ID,line FROM users WHERE id = '$id'");
	
	if (!$result){
		echo 'Could not run query: ' . mysqli_error();
		exit;
	}
	
	$row = mysqli_fetch_row($result);
	
	//formatteren date data
	$dm = date_create($row[2]);
	$ls = date_create($row[3]);

?>
<?php

	include "PHP_LIBARY/valideer_blok.php";

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Neyos ~ Info</title>
		<link rel="shortcut icon" href="xoneyos.ico">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php // ADMIN layout
		
		if ($row[4] == 1){
			
			echo "<link rel='stylesheet' href='main.css'>";
			
		} else {
			
			echo "<link rel='stylesheet' href='main2.css'>";
			
		}
		
		?>
	</head>
	<body class="bg">
	
	<div align="center">
	
		<form>
		
		<img src="logo.png" alt="Logo isn't showing" class="logo">
		
		<p style="font-size:20px;"><?php echo $row[0]; ?></p>
		
		
		<?php // ADMIN text
		
		if ($row[4] == 1){
			
			echo "<p style='font-size:10px;'>A D M I N I S T R A T O R</p>";
			
		}
		
		?>
		
		
		
		<!-- Live profiel info -->
		<div id="live">
		
			<script>
			
			function live(){
				xmlhttp = new XMLHttpRequest();
				xmlhttp.open("GET","live_profielinfo?id=<?php echo $row[4]; ?>",false);
				xmlhttp.send(null);
				
				document.getElementById("live").innerHTML = xmlhttp.responseText;
			}
			
			live();
			
			//array voor maand
			var month = new Array();
			month[0] = "01";
			month[1] = "02";
			month[2] = "03";
			month[3] = "04";
			month[4] = "05";
			month[5] = "06";
			month[6] = "07";
			month[7] = "08";
			month[8] = "09";
			month[9] = "10";
			month[10] = "11";
			month[11] = "12";
	
			var vndg_892x = new Date();
			var gstrr_L268px = new Date(new Date().setDate(new Date().getDate()-1));
	
			var vdge__2 = vndg_892x.getDate();
			if (vdge__2 < 10){
				var vdge__2 = "0" + vdge__2;
			}
	
			var IOJMI9O9 = gstrr_L268px.getDate();
			if (IOJMI9O9 < 10){
				var IOJMI9O9 = "0" + IOJMI9O9;
			}
	
			var Mnthk9r3 = month[vndg_892x.getMonth()];
			var g_m = month[gstrr_L268px.getMonth()];
	
			var v_j = vndg_892x.getFullYear();
			var j30xxm = gstrr_L268px.getFullYear();
	
			var v_nd = vdge__2 + "/" + Mnthk9r3 + "/" + v_j;
			var g_nd = IOJMI9O9 + "/" + g_m + "/" + j30xxm;
	
	
			function update_time(){
				
				document.getElementById("ls").innerHTML = document.getElementById("ls").innerHTML.replace(v_nd, 'vandaag');
				document.getElementById("ls").innerHTML = document.getElementById("ls").innerHTML.replace(g_nd, 'gisteren');
				
			}
			
			update_time();
			
			setInterval(function(){
				live();
				update_time();
			},6000);
			
			</script>
		
		</div>
		<!-- Live profiel info -->
		
		
		
		<hr style="clear: both; visibility: hidden;">
		
		<p style="font-size:13px;">Unique-ID: <?php echo $row[4]; ?></p>
		
		<input type="button" name="chatten" value="Chatten" onclick="location.href='chat?id=<?php echo $row[4]; ?>'" style="width: 83px; background-color: #ffff00;"/>
		<input type="button" name="blokkeren" value="Blokkeren" onclick="location.href='blok?id=<?php echo $row[4]; ?>'" style="width: 83px; background-color: #ff4d4d;"/>
		
		<input type="button" name="terug" value="Terug" onclick="location.href='zoek'" style="width: 180px;"/>
		
		</form>
		
	</div>
		
	</body>
</html>