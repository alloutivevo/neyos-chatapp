<?php

	include "PHP_LIBARY/db_config.php";
	
	include "PHP_LIBARY/valideer_sessie.php";
	
	include "PHP_LIBARY/update_account_actief.php";
	
	include "PHP_LIBARY/update_ip_recent.php";
	
	include "PHP_LIBARY/update_date_recent.php";
		
?>

<?php

	//ophalen gebruikersgegevens
	$result = mysqli_query($connection, "SELECT ID FROM users WHERE email = '$email'");
	if (!$result) {
		echo 'Could not run query: ' . mysqli_error();
		exit;
	}
	
	$row = mysqli_fetch_row($result);
	$id_blokker = $row[0];
	
	//ophalen van GET input
	$id_geblokt = mysqli_real_escape_string($connection, $_GET["id"]);
	
	//filteren voor mogelijke SQL Injections
	if (strpos($id_geblokt, 'a') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id_geblokt, 'A') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id_geblokt, 'i') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id_geblokt, 'I') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id_geblokt, 'e') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id_geblokt, 'E') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id_geblokt, 'o') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id_geblokt, 'O') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id_geblokt, 'u') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id_geblokt, 'U') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id_geblokt, '-') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id_geblokt, '*') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id_geblokt, ' ') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	$id_geblokt = preg_replace("#[^0-9a-z]#i","",$id_geblokt);
	
	//valideer of ID geldig
	$query = "SELECT * FROM users WHERE id = '$id_geblokt'";
	$run = mysqli_query($connection, $query);
	$hit = mysqli_fetch_array($run);
	
	if ($hit < 1){
		Header('Location: error');
		exit;
	}

?>

<?php

	if($id_geblokt == $id_blokker){
		
		Header('Location: error');
		exit;
		
	}

	$query = "SELECT * FROM blok WHERE id_blokker = '$id_blokker' AND id_geblokt = '$id_geblokt'";
	$run = mysqli_query($connection, $query);
	
	if (mysqli_num_rows($run) > 0){
		
		Header("location:blokkeren");
		
	} else {

	$query = "INSERT INTO blok (id_blokker, id_geblokt) 
      	    	  VALUES ('$id_blokker', '$id_geblokt')";
				  
	$run = mysqli_query($connection, $query);
	
	Header("location:blokkeren");
	
	}
 
?>