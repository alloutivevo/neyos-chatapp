<!--
 　 · ✵						 　 · ✵					 　 · ✵					
 　　　　 ⋆ ✧　 　 · 　 ✧　✵	 　 · ✵					 　　　　 ⋆ ✧　 　 · 　 ✧　✵	
　　 ⋆ ✧　 　 · 　 ✧　✵		 。　☆ 。　　☆。　　☆ 	　　 ⋆ ✧　 　 · 　 ✧　✵		
 　 · ✵					★。　＼　　｜　　／。　★	 　 · ✵					
 　   *　　 * ⋆ 　 .		☆ 　　N E Y O S 		 　 　　 *　　 * ⋆ 　 .		
 · 　　 ⋆ 　　　 ˚ ˚ 　✦★。　／　　｜　　＼。　★  · 　　 ⋆ 　　　 ˚ ˚ 　　 ✦	
 　 ⋆ · 　 *				。　☆。 　　。　　☆。	 　 ⋆ · 　 *				
 　　　　 ⋆ ✧　 　 · 　 ✧　✵	 　 · ✵					　　　　 ⋆ ✧　 　 · 　 ✧　✵	
 　 · ✵					　 · ✵				 　 · ✵						
		Encryptie van heden is niet meer als toen en is toe aan vernieuwing.
		Author:	Anouar Allouti
-->
<?php

	include "PHP_LIBARY/db_config.php";
	
	include "PHP_LIBARY/valideer_sessie.php";
	
	include "PHP_LIBARY/update_account_actief.php";
	
	include "PHP_LIBARY/update_ip_recent.php";
	
	include "PHP_LIBARY/update_date_recent.php";
	
?>
<?php

	//ophalen gebruikersgegevens
	$result = mysqli_query($connection, "SELECT ID FROM users WHERE email = '$email'");
	if (!$result) {
		echo 'Could not run query: ' . mysqli_error();
		exit;
	}
	
	$row = mysqli_fetch_row($result);
	$id_ontvanger = $row[0];

	$query = "SELECT id, id_verzender, date FROM msg WHERE id_ontvanger = '$id_ontvanger' AND status = 1 ORDER BY id DESC";
	$result = mysqli_query($connection, $query);

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Neyos ~ Berichten</title>
		<link rel="shortcut icon" href="xoneyos.ico">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="main2.css">
	</head>
	<body class="bg">
	
	<div align="center">
	
		<form name="submit" method="POST" enctype="multipart/form-data" action="results">
		
			<img src="logo.png" alt="Logo isn't showing" class="logo">
			
			<p style="font-size: 17px;">Nieuwe berichten</p>
			
			<?php

					$hit = 0;
					
				
					// als resultaten
					while($gegevens = mysqli_fetch_row($result)){
							
							$hit = 1;
							
							$id					= $gegevens[0];
							$id_verzender 		= $gegevens[1];
							
							$button = "<input type='button' name='chat' value='Chatten' onclick='chat$id_verzender();' style='width: 180px; background-color: #ffff00;'/>";
							
							$nn = mysqli_query($connection, "SELECT nickname FROM users WHERE id = '$id_verzender'");
							$nn_r = mysqli_fetch_row($nn);
							
							$nickname = $nn_r[0];
							$date = date_create($gegevens[2]);
							
							echo "<table style='text-align: center; font-family:Trebuchet MS, Helvetica, sans-serif;'>";
							
								echo "<tr>";
									echo "<th>Nickname</th>";
									echo "<th>Ontvangen op</th>";
								echo "</tr>";
							
								echo "<tr>";
									echo "<td>$nickname</td>";
									
									echo "<td>";
										echo date_format($date, 'd/m/Y');
									echo "</td>";
								echo "</tr>";
								
								echo "<tr>";
									echo "<td colspan='3'>";
										echo $button;
									echo "</td>";
								echo "</tr>";

							echo "</table>";
							
							echo "<br>";
							
							echo "<script>	function chat$id_verzender(){ location.href='chat?id=$id_verzender' }	</script>";
							
							}
							
							
					// als geen resultaten
					if($hit == 0){
						
							echo "<p class='text'>(Geen nieuwe berichten)</p>";
							
					} else {
						
						// niks
						
					}
					
			?>
			
			<input type="button" name="terug" value="Terug" onclick="location.href='home'" style="width: 180px;"/>
		
		</form>

	</div>
		
	</body>
</html>