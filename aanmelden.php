<!--
 　 · ✵						 　 · ✵					 　 · ✵					
 　　　　 ⋆ ✧　 　 · 　 ✧　✵	 　 · ✵					 　　　　 ⋆ ✧　 　 · 　 ✧　✵	
　　 ⋆ ✧　 　 · 　 ✧　✵		 。　☆ 。　　☆。　　☆ 	　　 ⋆ ✧　 　 · 　 ✧　✵		
 　 · ✵					★。　＼　　｜　　／。　★	 　 · ✵					
 　   *　　 * ⋆ 　 .		☆ 　　N E Y O S 		 　 　　 *　　 * ⋆ 　 .		
 · 　　 ⋆ 　　　 ˚ ˚ 　✦★。　／　　｜　　＼。　★  · 　　 ⋆ 　　　 ˚ ˚ 　　 ✦	
 　 ⋆ · 　 *				。　☆。 　　。　　☆。	 　 ⋆ · 　 *				
 　　　　 ⋆ ✧　 　 · 　 ✧　✵	 　 · ✵					　　　　 ⋆ ✧　 　 · 　 ✧　✵	
 　 · ✵					　 · ✵				 　 · ✵						
		Encryptie van heden is niet meer als toen en is toe aan vernieuwing.
		Author:	Anouar Allouti
-->
<?php 

  $db = mysqli_connect('localhost', 'root', 'Nouranoura777', 'neyos');
  
  $ip_made = $_SERVER['REMOTE_ADDR'];
  
	//valideer sessie
	session_start();
	if(isset($_SESSION['email'])){
		header("location: home");
		echo "<script>console.log('sessie actief');</script>";
	} else {
		echo "<script>console.log('sessie onactief');</script>";
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Neyos ~ Aanmelden</title>
		<link rel="shortcut icon" href="xoneyos.ico">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="main2.css">
	</head>
	<body class="bg">
	
	<div align="center">
	
		<form method="POST" enctype="multipart/form-data">
		
			<img src="logo.png" alt="Logo isn't showing" class="logo">
			
			<input required type="text"
			name="nickname" placeholder="nickname" maxlength="20" style="width: 180px;"/><br>
			
			<input required type="email"
			name="email" placeholder="email@voorbeeld.nl" maxlength="50" style="width: 180px;"/><br>
			
			<input required type="password"
			name="password" placeholder="wachtwoord" style="width: 180px;"/><br>

			<input required type="password"
			name="reppassword" placeholder="herhaal wachtwoord" style="width: 180px;"/><br>
			
			<input type="submit"
			name="submit" value=" Aanmelden " style="width: 83px;"/>
			
			<input type="button"
			name="annuleren" value=" Annuleren " style="width: 83px;" onclick="location.href='inloggen'"/>
			
			<p style="color: #FF0000;" id="melding"></p>
			
		</form>

	</div>
		
	</body>
</html>
<?php

	//aanmelden form
	if(isset($_POST['submit'])) {
		
  	$nickname = $_POST['nickname'];
  	$email = $_POST['email'];
  	$password = $_POST['password'];
	$reppassword = $_POST['reppassword'];
	
	if ($password !== $reppassword){
		
		echo "<script>document.getElementById('melding').innerHTML = 'Wachtwoorden komen niet overeen.'</script>";
		exit();
		
	} else {
		
		// niks
		
	}
	
	include "sleutels/kryptos.php";

  	$sql_u = "SELECT * FROM users WHERE nickname='$nickname'";
  	$sql_e = "SELECT * FROM users WHERE email='$email'";
  	$res_u = mysqli_query($db, $sql_u);
  	$res_e = mysqli_query($db, $sql_e);

  	if (mysqli_num_rows($res_u) > 0){
		
		echo "<script>document.getElementById('melding').innerHTML = 'Nickname is bezet.'</script>"; 
		
	}else if(mysqli_num_rows($res_e) > 0){  
	
		echo "<script>document.getElementById('melding').innerHTML = 'E-mail is bezet.'</script>"; 
		
	}else{
           $query = "INSERT INTO users (nickname, email, actief, ip_made, password) 
      	    	  VALUES ('$nickname', '$email', '0', '$ip_made', '$password')";
				  
           $results = mysqli_query($db, $query);
		   
		   echo "<script>alert('Account aangemaakt!');</script>"; 
		   echo "<script>location.href='inloggen'</script>";
		   
           exit();
  	}
	}

?>