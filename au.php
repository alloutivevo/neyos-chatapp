<!--
 　 · ✵						 　 · ✵					 　 · ✵					
 　　　　 ⋆ ✧　 　 · 　 ✧　✵	 　 · ✵					 　　　　 ⋆ ✧　 　 · 　 ✧　✵	
　　 ⋆ ✧　 　 · 　 ✧　✵		 。　☆ 。　　☆。　　☆ 	　　 ⋆ ✧　 　 · 　 ✧　✵		
 　 · ✵					★。　＼　　｜　　／。　★	 　 · ✵					
 　   *　　 * ⋆ 　 .		☆ 　　N E Y O S 		 　 　　 *　　 * ⋆ 　 .		
 · 　　 ⋆ 　　　 ˚ ˚ 　✦★。　／　　｜　　＼。　★  · 　　 ⋆ 　　　 ˚ ˚ 　　 ✦	
 　 ⋆ · 　 *				。　☆。 　　。　　☆。	 　 ⋆ · 　 *				
 　　　　 ⋆ ✧　 　 · 　 ✧　✵	 　 · ✵					　　　　 ⋆ ✧　 　 · 　 ✧　✵	
 　 · ✵					　 · ✵				 　 · ✵						
		Encryptie van heden is niet meer als toen en is toe aan vernieuwing.
		Author:	Anouar Allouti
-->
<?php

	include "PHP_LIBARY/db_config.php";
	
	include "PHP_LIBARY/valideer_sessie.php";
	
	include "PHP_LIBARY/update_account_actief.php";
	
	include "PHP_LIBARY/update_ip_recent.php";
	
	include "PHP_LIBARY/update_date_recent.php";
	
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Neyos ~ AU</title>
		<link rel="shortcut icon" href="xoneyos.ico">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="main2.css">
	</head>
	<body class="bg">
	
	<div align="center">
	
		<form name="submit" method="POST" enctype="multipart/form-data">
		
			<img src="logo.png" alt="Logo isn't showing" class="logo">
			
			<p style="font-size:10px;">Bij het voltooien van dit formulier kunnen andere gebruikers uw profiel en voorafgaande conversatie(s) <span style="color:red;font-weight:bold;">niet</span> meer weergeven. U dient uw account opnieuw te activeren om weer te kunnen inloggen.</p>
			<br>
			
			<input required type="password"
			name="password" placeholder="wachtwoord" style="width: 180px;"/><br>

			<input required type="password"
			name="reppassword" placeholder="herhaal wachtwoord" style="width: 180px;"/><br>
			
			<input type="submit"
			name="submit" value="Bevestigen" style="width: 79px;" />
			
			<input type="button"
			name="annuleren" value="Annuleren" onclick="location.href='home'" style="width: 79px;"/>
			
			<p style="color: #FF0000;" id="melding"></p>
			
		</form>

	</div>
		
	</body>
</html>
<?php

	if(isset($_POST['submit'])) {
		
		$password 		= mysqli_real_escape_string($connection, $_POST['password']);
		$reppassword 	= mysqli_real_escape_string($connection, $_POST['reppassword']);
		
		// check of velden ingevuld zijn
		if(empty($_POST["password"]) || empty($_POST["reppassword"]))
		{  
	
           echo "<script>document.getElementById('melding').innerHTML = 'Veld(en) niet ingevuld.'</script>"; 
		   exit();
		   
		} else {
			
			// niks
			
		}
		
		// check of wachtwoord overeen komt met herhaal wachtwoord veld
		if ($password !== $reppassword){
		
		echo "<script>document.getElementById('melding').innerHTML = 'Wachtwoorden komen niet overeen.'</script>";
		exit();
		
		} else {
		
			// niks
		
		}
		
		include "sleutels/kryptos.php";
		
		// check of wachtwoord juist is
		$query = "SELECT password FROM users WHERE email = '$email'";
		$result = mysqli_query($connection, $query);
		$ftch = mysqli_fetch_row($result);
		
		if ($ftch[0] !== $password){
		
		echo "<script>document.getElementById('melding').innerHTML = 'Wachtwoord onjuist.'</script>";
		exit();
		
		} else {
		
			// niks
			
		}
		
		// als bovenste 3 processen correct afgehandelt zijn wordt de gebruiker on-actief gezet
		$query = "UPDATE users SET actief = '0' WHERE email = '$email'";
		$run = mysqli_query($connection, $query);
		
		Header("Location: logout");
		
	}

?>