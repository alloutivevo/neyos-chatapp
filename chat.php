<!--
 　 · ✵						 　 · ✵					 　 · ✵					
 　　　　 ⋆ ✧　 　 · 　 ✧　✵	 　 · ✵					 　　　　 ⋆ ✧　 　 · 　 ✧　✵	
　　 ⋆ ✧　 　 · 　 ✧　✵		 。　☆ 。　　☆。　　☆ 	　　 ⋆ ✧　 　 · 　 ✧　✵		
 　 · ✵					★。　＼　　｜　　／。　★	 　 · ✵					
 　   *　　 * ⋆ 　 .		☆ 　　N E Y O S 		 　 　　 *　　 * ⋆ 　 .		
 · 　　 ⋆ 　　　 ˚ ˚ 　✦★。　／　　｜　　＼。　★  · 　　 ⋆ 　　　 ˚ ˚ 　　 ✦	
 　 ⋆ · 　 *				。　☆。 　　。　　☆。	 　 ⋆ · 　 *				
 　　　　 ⋆ ✧　 　 · 　 ✧　✵	 　 · ✵					　　　　 ⋆ ✧　 　 · 　 ✧　✵	
 　 · ✵					　 · ✵				 　 · ✵						
		Encryptie van heden is niet meer als toen en is toe aan vernieuwing.
		Author:	Anouar Allouti
-->
<?php

	include "PHP_LIBARY/db_config.php";
	
	include "PHP_LIBARY/valideer_sessie.php";
	
	include "PHP_LIBARY/update_account_actief.php";
	
	include "PHP_LIBARY/update_ip_recent.php";
	
	include "PHP_LIBARY/update_date_recent.php";
	
?>
<?php

	//ophalen van GET input
	$id = mysqli_real_escape_string($connection, $_GET["id"]);
	
	//filteren voor mogelijke SQL Injections
	
	if (strpos($id, 'a') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'A') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'i') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'I') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'e') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'E') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'o') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'O') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'u') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'U') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, '-') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, '*') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, ' ') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	$id = preg_replace("#[^0-9a-z]#i","",$id);
	
	//valideer of ID geldig
	$query = "SELECT * FROM users WHERE id = '$id'";
	$run = mysqli_query($connection, $query);
	$hit = mysqli_fetch_array($run);
	
	if ($hit < 1){
		Header('Location: error');
		exit;
	}
	
	//valideer of user actief
	$query = "SELECT actief FROM users WHERE id = '$id'";
	$run = mysqli_query($connection, $query);
	$hit = mysqli_fetch_array($run);
	
	if ($hit[0] == 0){
		Header('Location: error');
		exit;
	}
	
	//ophalen gebruikersgegevens
	$result = mysqli_query($connection, "SELECT nickname,foto,date_made,date_recent,ID,line FROM users WHERE id = '$id'");
	
	if (!$result){
		echo 'Could not run query: ' . mysqli_error();
		exit;
	}
	
	$row = mysqli_fetch_row($result);
	
	//formatteren date data
	$dm = date_create($row[2]);
	$ls = date_create($row[3]);

?>

<?php
	
	//ophalen id van ontvanger & verzender
	$sql_id = mysqli_query($connection, "SELECT id FROM users WHERE email = '$email'");
	$res_id = mysqli_fetch_row($sql_id);
		
	$id_verzender = $res_id[0];
	$id_ontvanger = $row[4];

?>

<?php

	include "PHP_LIBARY/valideer_blok.php";

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Neyos ~ Chat</title>
		<link rel="shortcut icon" href="xoneyos.ico">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="main2.css">
	</head>
	<body class="bg">
	
	<div align="center">
	
		<form name="verstuur" method="POST" enctype="multipart/form-data">
		
		<img src="logo.png" alt="Logo isn't showing" class="logo">
		
		<div id="photo">
		
			<script>
			
			function photo(){
				xmlhttp = new XMLHttpRequest();
				xmlhttp.open("GET","live_profielfoto?id=<?php echo $id_ontvanger; ?>",false);
				xmlhttp.send(null);
				
				document.getElementById("photo").innerHTML = xmlhttp.responseText;
			}
			
			photo();
			
			//array voor maand
			var month = new Array();
			month[0] = "01";
			month[1] = "02";
			month[2] = "03";
			month[3] = "04";
			month[4] = "05";
			month[5] = "06";
			month[6] = "07";
			month[7] = "08";
			month[8] = "09";
			month[9] = "10";
			month[10] = "11";
			month[11] = "12";
	
			var vndg_892x = new Date();
			var gstrr_L268px = new Date(new Date().setDate(new Date().getDate()-1));
	
			var vdge__2 = vndg_892x.getDate();
			if (vdge__2 < 10){
				var vdge__2 = "0" + vdge__2;
			}
	
			var IOJMI9O9 = gstrr_L268px.getDate();
			if (IOJMI9O9 < 10){
				var IOJMI9O9 = "0" + IOJMI9O9;
			}
	
			var Mnthk9r3 = month[vndg_892x.getMonth()];
			var g_m = month[gstrr_L268px.getMonth()];
	
			var v_j = vndg_892x.getFullYear();
			var j30xxm = gstrr_L268px.getFullYear();
	
			var v_nd = vdge__2 + "/" + Mnthk9r3 + "/" + v_j;
			var g_nd = IOJMI9O9 + "/" + g_m + "/" + j30xxm;
	
	
			function update_time(){
				
				document.getElementById("ls").innerHTML = document.getElementById("ls").innerHTML.replace(v_nd, 'vandaag');
				document.getElementById("ls").innerHTML = document.getElementById("ls").innerHTML.replace(g_nd, 'gisteren');
				
			}
			
			update_time();
			
			setInterval(function(){
				photo();
				update_time();
			},8000);
			
			</script>
		
		</div>

		<input type="button" name="info" value="Info" onclick="location.href='info?id=<?php echo $row[4]; ?>'" style="width: 83px; background-color: #00e6e6;"/>
		<input type="button" name="blokkeren" value="Blokkeren" onclick="location.href='blok?id=<?php echo $row[4]; ?>'" style="width: 83px; background-color: #ff4d4d;"/>
		
		
		
		<div id="head" style="background-image: url('bgchat.jpg'); background-position: center; background-repeat: no-repeat; background-size: cover; width: 180px; height: 200px; border: 1px solid black; border-radius: 4px 4px 4px 4px; overflow: auto; font-size: 11px;">
		
			<script>
			
			function dis(){
				xmlhttp = new XMLHttpRequest();
				xmlhttp.open("GET","log?id=<?php echo $id_ontvanger; ?>",false);
				xmlhttp.send(null);
				
				document.getElementById("head").innerHTML = xmlhttp.responseText;
			}
			
			dis();
			
			setInterval(function(){
				dis();
			},3000);
			
			</script>
			
		</div>
		
		
		<textarea placeholder="Klik hier om een bericht te typen..." style="height: 70px; width: 180px; resize:none; border: 1px solid black;" name="bericht" maxlength="600"></textarea>
			
		<input type="submit" name="verstuur" value="Verstuur" style="width: 83px; background-color: #ffff00;"/>
		<input type="button" name="terug" value="Terug" onclick="location.href='zoek'" style="width: 83px;"/>
		
		
		<!-- Hierboven chat gedeelte -->
	
		</form>
		
	</div>
		
	</body>
</html>

<?php

	if(isset($_POST['verstuur'])){ 
	
		$bericht = mysqli_real_escape_string($connection, $_POST["bericht"]);  
	
		if(!$bericht){
			
			// als bericht geen inhoud niks versturen
			
		} else {
		
		//encrypt
		include "sleutels/encoder.php";
		
		$run = mysqli_query($connection, "SELECT * FROM msg WHERE id_verzender = '$id_verzender' AND id_ontvanger = '$id_ontvanger' AND status = 1");
		
		if(mysqli_num_rows($run) > 0){
			
		$query = "INSERT INTO msg (id_verzender, id_ontvanger, bericht, status) 
      	    	  VALUES ('$id_verzender', '$id_ontvanger', '$bericht', 0)";
				  
		} else {
			
		$query = "INSERT INTO msg (id_verzender, id_ontvanger, bericht, status) 
      	    	  VALUES ('$id_verzender', '$id_ontvanger', '$bericht', 1)";
			
		}
		
		$send = mysqli_query($connection, $query);
		
		}
		
	}

?>