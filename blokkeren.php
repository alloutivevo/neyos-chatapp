<!--
 　 · ✵						 　 · ✵					 　 · ✵					
 　　　　 ⋆ ✧　 　 · 　 ✧　✵	 　 · ✵					 　　　　 ⋆ ✧　 　 · 　 ✧　✵	
　　 ⋆ ✧　 　 · 　 ✧　✵		 。　☆ 。　　☆。　　☆ 	　　 ⋆ ✧　 　 · 　 ✧　✵		
 　 · ✵					★。　＼　　｜　　／。　★	 　 · ✵					
 　   *　　 * ⋆ 　 .		☆ 　　N E Y O S 		 　 　　 *　　 * ⋆ 　 .		
 · 　　 ⋆ 　　　 ˚ ˚ 　✦★。　／　　｜　　＼。　★  · 　　 ⋆ 　　　 ˚ ˚ 　　 ✦	
 　 ⋆ · 　 *				。　☆。 　　。　　☆。	 　 ⋆ · 　 *				
 　　　　 ⋆ ✧　 　 · 　 ✧　✵	 　 · ✵					　　　　 ⋆ ✧　 　 · 　 ✧　✵	
 　 · ✵					　 · ✵				 　 · ✵						
		Encryptie van heden is niet meer als toen en is toe aan vernieuwing.
		Author:	Anouar Allouti
-->
<?php

	include "PHP_LIBARY/db_config.php";
	
	include "PHP_LIBARY/valideer_sessie.php";
	
	include "PHP_LIBARY/update_account_actief.php";
	
	include "PHP_LIBARY/update_ip_recent.php";
	
	include "PHP_LIBARY/update_date_recent.php";
		
?>
<?php
	
	//ophalen gebruikersgegevens
	$result = mysqli_query($connection, "SELECT ID FROM users WHERE email = '$email'");
	if (!$result) {
		echo 'Could not run query: ' . mysqli_error();
		exit;
	}
	
	$row = mysqli_fetch_row($result);
	$id_blokker = $row[0];

	$query = "SELECT id, id_geblokt, date FROM blok WHERE id_blokker = '$id_blokker' ORDER BY id DESC";
	$result = mysqli_query($connection, $query);

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Neyos ~ Bloklijst</title>
		<link rel="shortcut icon" href="xoneyos.ico">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="main2.css">
	</head>
	<body class="bg">
	
	<div align="center">
	
		<form>
		
			<img src="logo.png" alt="Logo isn't showing" class="logo">
						
			<p style="font-size: 17px;">Geblokkeerde gebruikers</p>
			
			<?php 
					$hit = 0;
					
				
					// als resultaten
					while($gegevens = mysqli_fetch_row($result)){
							
							$hit = 1;
							
							$id					= $gegevens[0];
							$id_geblokt 		= $gegevens[1];
							
							echo "<script>	function deblok$id(){ location.href='deblok?id=$id' }	</script>";
							$button = "<input type='button' name='deblok' value='Deblokkeer' onclick='deblok$id();' style='width: 180px; background-color: #ffa31a;'/>";
							
							$nn = mysqli_query($connection, "SELECT nickname FROM users WHERE id = '$id_geblokt'");
							$nn_r = mysqli_fetch_row($nn);
							
							$nickname = $nn_r[0];
							$date = date_create($gegevens[2]);
							
							echo "<table style='text-align: center; font-family:Trebuchet MS, Helvetica, sans-serif;'>";
							
								echo "<tr>";
									echo "<th>Nickname</th>";
									echo "<th>Geblokt op</th>";
								echo "</tr>";
							
								echo "<tr>";
									echo "<td>$nickname</td>";
									
									echo "<td>";
										echo date_format($date, 'd/m/Y');
									echo "</td>";
								echo "</tr>";
								
								echo "<tr>";
									echo "<td colspan='3'>";
										echo $button;
									echo "</td>";
								echo "</tr>";

							echo "</table>";
							
							echo "<br>";
							
							}
							
							
					// als geen resultaten
					if($hit == 0){
						
							echo "<p class='text'>(Geen gebruikers geblokkeerd)</p>";
							
					} else {
						
						// niks
						
					}
			?>
			
			<br>
			
		<input type="button" value="Terug" onclick="location.href='home'" style="width: 180px;"/>	
			
		</form>
		
		

		<br><br>
		
	</div>
		
	</body>
</html>