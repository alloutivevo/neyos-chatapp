<!--
 　 · ✵						 　 · ✵					 　 · ✵					
 　　　　 ⋆ ✧　 　 · 　 ✧　✵	 　 · ✵					 　　　　 ⋆ ✧　 　 · 　 ✧　✵	
　　 ⋆ ✧　 　 · 　 ✧　✵		 。　☆ 。　　☆。　　☆ 	　　 ⋆ ✧　 　 · 　 ✧　✵		
 　 · ✵					★。　＼　　｜　　／。　★	 　 · ✵					
 　   *　　 * ⋆ 　 .		☆ 　　N E Y O S 		 　 　　 *　　 * ⋆ 　 .		
 · 　　 ⋆ 　　　 ˚ ˚ 　✦★。　／　　｜　　＼。　★  · 　　 ⋆ 　　　 ˚ ˚ 　　 ✦	
 　 ⋆ · 　 *				。　☆。 　　。　　☆。	 　 ⋆ · 　 *				
 　　　　 ⋆ ✧　 　 · 　 ✧　✵	 　 · ✵					　　　　 ⋆ ✧　 　 · 　 ✧　✵	
 　 · ✵					　 · ✵				 　 · ✵						
		Encryptie van heden is niet meer als toen en is toe aan vernieuwing.
		Author:	Anouar Allouti
-->
<?php

	include "PHP_LIBARY/db_config.php";
	
	include "PHP_LIBARY/valideer_sessie.php";
	
	include "PHP_LIBARY/update_account_actief.php";
	
	include "PHP_LIBARY/update_ip_recent.php";
	
	include "PHP_LIBARY/update_date_recent.php";
	
?>
<?php

	//ophalen gebruikers_id voor <div id="liveberichten">
	$result = mysqli_query($connection, "SELECT ID FROM users WHERE email = '$email'");
	if (!$result) {
		echo 'Could not run query: ' . mysqli_error();
		exit;
	}
	
	$row = mysqli_fetch_row($result);

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Neyos ~ Zoeken</title>
		<link rel="shortcut icon" href="xoneyos.ico">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="main2.css">
	</head>
	<body class="bg">
	
	<div align="center">
	
		<form name="submit" method="POST" enctype="multipart/form-data" action="results">
		
			<img src="logo.png" alt="Logo isn't showing" class="logo">
			
			<span style="font-family:'Trebuchet MS', Helvetica, sans-serif; font-size:20px; align:center;">Chatten</span>
			
			<div id="liveberichten">
				<script>
			
				function liveberichten(){
					xmlhttp = new XMLHttpRequest();
					xmlhttp.open("GET","live_berichtenhome?id=<?php echo $row[0]; ?>",false);
					xmlhttp.send(null);
				
					document.getElementById("liveberichten").innerHTML = xmlhttp.responseText;
				}
			
				liveberichten();
				
				setInterval(function(){
					liveberichten();
				},6000);
				
				</script>
			</div>
			
			<input required type="text"
			name="nickname" placeholder="nickname" maxlength="20" style="width: 169px;"/><br>
			
			<input type="submit"
			name="submit" value="Zoeken" style="width: 79px;" />
			
			<input type="button"
			name="annuleren" value="Annuleren" onclick="location.href='home'" style="width: 79px;"/>
			
		</form>

	</div>
		
	</body>
</html>