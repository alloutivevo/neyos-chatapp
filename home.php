<!--
 　 · ✵						 　 · ✵					 　 · ✵					
 　　　　 ⋆ ✧　 　 · 　 ✧　✵	 　 · ✵					 　　　　 ⋆ ✧　 　 · 　 ✧　✵	
　　 ⋆ ✧　 　 · 　 ✧　✵		 。　☆ 。　　☆。　　☆ 	　　 ⋆ ✧　 　 · 　 ✧　✵		
 　 · ✵					★。　＼　　｜　　／。　★	 　 · ✵					
 　   *　　 * ⋆ 　 .		☆ 　　N E Y O S 		 　 　　 *　　 * ⋆ 　 .		
 · 　　 ⋆ 　　　 ˚ ˚ 　✦★。　／　　｜　　＼。　★  · 　　 ⋆ 　　　 ˚ ˚ 　　 ✦	
 　 ⋆ · 　 *				。　☆。 　　。　　☆。	 　 ⋆ · 　 *				
 　　　　 ⋆ ✧　 　 · 　 ✧　✵	 　 · ✵					　　　　 ⋆ ✧　 　 · 　 ✧　✵	
 　 · ✵					　 · ✵				 　 · ✵						
		Encryptie van heden is niet meer als toen en is toe aan vernieuwing.
		Author:	Anouar Allouti
-->
<?php   
	
	include "PHP_LIBARY/valideer_sessie.php";
	
	include "PHP_LIBARY/db_config.php";
	
	include "PHP_LIBARY/update_account_actief.php";

	//ophalen gebruikersgegevens
	$result = mysqli_query($connection, "SELECT nickname,foto,ID FROM users WHERE email = '$email'");
	if (!$result) {
		echo 'Could not run query: ' . mysqli_error();
		exit;
	}
	
	$row = mysqli_fetch_row($result);
	
	include "PHP_LIBARY/update_ip_recent.php";
	
	include "PHP_LIBARY/update_date_recent.php";
	
?>

<?php
if (isset($_POST['submit'])){
	
	if(empty($_FILES["file"]["name"])){
		
	move_uploaded_file($_FILES["file"]["tmp_name"], "fotos/" . $_FILES["file"]["name"]);
	$q = mysqli_query($connection,"UPDATE users SET foto = '".$_FILES["file"]["name"]."' WHERE email = '".$_SESSION['email']."'");
	
	} else {
		
	//check of file jpg,jpeg,png of gif is
	$extension = pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);
		if($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png' || $extension == 'gif'){
			
			$temp = explode(".", $_FILES["file"]["name"]);
			$newfilename = round(microtime(true)) . '.' . end($temp);
			move_uploaded_file($_FILES["file"]["tmp_name"], "fotos/" . $newfilename);
			$q = mysqli_query($connection,"UPDATE users SET foto = '".$newfilename."' WHERE email = '".$_SESSION['email']."'");
	
			echo "
	  
				<script>
	  
				vpf = alert('Profielfoto gewijzigd!');
				if (vpf = true){
					location = location
				} else {
					location = location }
			
				</script>
	  
			";
			
		} else {
			
			echo "
	  
				<script>
	  
				err = alert('Profielfoto geweigerd!');
				if (err = true){
					location = location
				} else {
					location = location }
			
				</script>
	  
			";
			
		}
	}
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Neyos ~ <?php echo $row[0]; ?></title>
		<link rel="shortcut icon" href="xoneyos.ico">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="main2.css">
		<script src="consolefunctions.js"></script>
	</head>
	<body class="bg">
	
	<div align="center">
	
		<form method="POST" enctype="multipart/form-data">
		
			<img src="logo.png" alt="Logo isn't showing" class="logo">

			<marquee behavior="scroll" direction="left" scrollamount="4"><span style="font-family:'Trebuchet MS', Helvetica, sans-serif; font-size:20px;">	<script>document.write(bericht);</script> <?php echo $row[0]; ?></span></marquee>
			
			<div id="liveberichten">
				<script>
			
				function liveberichten(){
					xmlhttp = new XMLHttpRequest();
					xmlhttp.open("GET","live_berichtenhome?id=<?php echo $row[2]; ?>",false);
					xmlhttp.send(null);
				
					document.getElementById("liveberichten").innerHTML = xmlhttp.responseText;
				}
			
				liveberichten();
				
				setInterval(function(){
					liveberichten();
				},6000);
				
				</script>
			</div>
	
			<?php 

			if($row[1] == ""){
				echo "<img src='default.jpg' alt='Foto is niet beschikbaar' style='width: 180px; border: 1px solid black;	border-radius: 4px 4px 4px 4px;' />";
			} else {
				echo "<img src='fotos/".$row[1]."' alt='Foto is niet beschikbaar' style='width: 180px; border: 1px solid black;	border-radius: 4px 4px 4px 4px;' />";
			}
			
			?>
			
			<!-- Hieronder Profielfoto gedeelte -->
			<input type="file" name="file" style="width: 180px;" accept="image/*">
			
			<input type="submit"
			name="submit" value="Profielfoto opslaan" style="width: 180px;"/>
			<!-- Hierboven Profielfoto gedeelte -->
			
			<input type="button"
			name="chat" value="Chatten" onclick="location.href='zoek'" style="width: 83px; background-color: #ffff00;"/>
			
			<input type="button"
			name="message" 	value="Bloklijst" onclick="location.href='blokkeren'" style="width: 83px; background-color: #ffa31a;"/>
			
			<input type="button"
			name="uitloggen" value="Uitloggen" onclick="location.href='logout'" style="width: 180px; background-color: #ff4d4d;"/>
			
		</form>
		
	</div>
		
	<br>	

	</body>
</html>