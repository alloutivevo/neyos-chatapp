<?php

	include "PHP_LIBARY/db_config.php";
	
	include "PHP_LIBARY/valideer_sessie.php";
	
	include "PHP_LIBARY/update_account_actief.php";
	
	include "PHP_LIBARY/update_ip_recent.php";
	
	include "PHP_LIBARY/update_date_recent.php";
	
?>

<?php

	//ophalen van GET input
	$id = mysqli_real_escape_string($connection, $_GET["id"]);
	
	//filteren voor mogelijke SQL Injections
	
	if (strpos($id, 'a') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'A') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'i') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'I') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'e') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'E') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'o') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'O') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'u') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, 'U') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, '-') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, '*') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	if (strpos($id, ' ') !== false) {
		// Header("Location: /info.php?id=$id");
		Header("Location: error");
	}
	
	$id = preg_replace("#[^0-9a-z]#i","",$id);
	
	//valideer of ID geldig
	$query = "SELECT * FROM users WHERE id = '$id'";
	$run = mysqli_query($connection, $query);
	$hit = mysqli_fetch_array($run);
	
	if ($hit < 1){
		Header('Location: error');
		exit;
	}
	
	//valideer of user actief
	$query = "SELECT actief FROM users WHERE id = '$id'";
	$run = mysqli_query($connection, $query);
	$hit = mysqli_fetch_array($run);
	
	if ($hit[0] == 0){
		Header('Location: error');
		exit;
	}
	
	//ophalen gebruikersgegevens
	$result = mysqli_query($connection, "SELECT nickname,foto,date_made,date_recent,ID,line FROM users WHERE id = '$id'");
	
	if (!$result){
		echo 'Could not run query: ' . mysqli_error();
		exit;
	}
	
	$row = mysqli_fetch_row($result);
	
	//formatteren date data
	$dm = date_create($row[2]);
	$ls = date_create($row[3]);

?>

<?php
	
	//ophalen id van ontvanger & verzender
	$sql_id = mysqli_query($connection, "SELECT id FROM users WHERE email = '$email'");
	$res_id = mysqli_fetch_row($sql_id);
		
	$id_verzender = $res_id[0];
	$id_ontvanger = $row[4];
	
?>

<?php
	
	$query = "SELECT bericht,id_verzender,date FROM msg WHERE id_verzender = '$id_verzender' AND id_ontvanger = '$id_ontvanger' OR id_verzender = '$id_ontvanger' AND id_ontvanger = '$id_verzender' ORDER BY id DESC";
	$run = mysqli_query($connection, $query);
	
	while($data = mysqli_fetch_row($run)){
		
		$bericht = $data[0];
		
		//decrypt
		include "sleutels/decoder.php";
		
		//berichten strings vervangen
		include "PHP_LIBARY/bericht_replacements.php";
		
		$zender = $data[1];
		
		$datum = $data[2];
		$dm = date_create($datum);
		
		$last = date_format($dm, 'd/m/Y H:i:s');
		
		// als verzender bericht verzender is
		if($zender == $id_verzender){
			
			echo "<p title='Verstuurd op: $last' style='border:1px solid black;border-radius: 4px 4px 4px 4px; background-color: #FFFFFF;'>";
			
			echo "<b>";
			echo "Ik: ";
			echo "</b>";
			
			echo $bericht;
			
			echo "</p>";
			
		// als ontvanger bericht verzender is
		} else {
			
			echo "<p title='Verstuurd op: $last' style='border:1px solid black;border-radius: 4px 4px 4px 4px; background-color: #d9fecd;'>";
			
			echo "<b>";
			echo "$row[0]: ";
			echo "</b>";
			
			echo $bericht;
			
			echo "</p>";
			
		}
	
	}
	
?>

<?php

	//Author; Anouar Allouti
	
	//de gebruiker die geblokt is zal de gebruiker die de ander geblokt heeft niet zien
	
	$start = mysqli_query($connection, "SELECT ID FROM users WHERE email = '$email'");
	if (!$start) {
		echo 'Could not run query: ' . mysqli_error();
		exit;
	}
	
	$eind = mysqli_fetch_row($start);
	$eigen_id = $eind[0];
	
	$query = mysqli_query($connection, "SELECT * FROM blok WHERE id_blokker = '$id' AND id_geblokt = '$eigen_id'");
	$eind = mysqli_fetch_array($query);
	
	if ($eind > 1){
		Header('Location: log_blanco');
		exit;
	} else {
		
		// niks
		
	}

?>

<?php

	//Author; Anouar Allouti
	
	//de gebruiker zal zijn nieuwe berichten hiermee doen omzetten van status 1 > 0
	
	$start = mysqli_query($connection, "UPDATE msg SET status = '0' WHERE id_verzender = '$id_ontvanger' AND id_ontvanger = '$id_verzender'");
	if (!$start) {
		echo 'Could not run query: ' . mysqli_error();
		exit;
	}
	
?>